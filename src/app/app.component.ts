import { HttpErrorResponse } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import * as HighCharts from "highcharts";
import { ChartsService } from "./service/charts.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "Angular 9 HighCharts";
  constructor(private chartApiService: ChartsService) { }

  ngOnInit() {
    this.getLastTwoWeeksOrderCount()
    this.getCurrentWeekSales()
    this.getCurrentWeekOrders()
  }

  getLastTwoWeeksOrderCount() {
    this.chartApiService.getFirstGraphData().subscribe((records: any) => {
      if (records.message === "success") {
        let orderCount = records.data.map((obj, index) => { return [`${index + 1}`, obj.orderCount] })
        this.barChartPopulation(orderCount);
      }
    }, (error: HttpErrorResponse) => {
      console.log(error);
    })
  }

  getCurrentWeekSales() {
    this.chartApiService.getSecondGraphData().subscribe((records: any) => {
      if (records.message === "success") {
        let lastWeekTotalSales = records.lastWeekData[0].tototalSale;
        let currentWeekTotalSales = records.currentWeekData[0].tototalSale;
        let percentageGrowthValue = (((currentWeekTotalSales - lastWeekTotalSales) / lastWeekTotalSales) * 100);
        let totalSales = records.currentWeekData[0].days.map((obj, index) => { return [Date.parse(obj._id), obj.product_price] });
        this.dailySalesCount(totalSales, percentageGrowthValue.toFixed(2), currentWeekTotalSales);
      }
    }, (error: HttpErrorResponse) => {
      console.log(error);
    })
  }

  getCurrentWeekOrders() {
    this.chartApiService.getThirdGraphData().subscribe((records: any) => {
      if (records.message === "success") {
        let lastWeekTotalSales = records.lastWeekData[0].tototalOrders;
        let currentWeekTotalSales = records.currentWeekData[0].tototalOrders;
        let percentageGrowthValue = (((currentWeekTotalSales - lastWeekTotalSales) / lastWeekTotalSales) * 100);
        let totalSales = records.currentWeekData[0].days.map((obj, index) => { return [Date.parse(obj._id), obj.orderCount] });
        this.dailyOrdersCount(totalSales, percentageGrowthValue.toFixed(2), currentWeekTotalSales);
      }
    }, (error: HttpErrorResponse) => {
      console.log(error);
    })
  }

  barChartPopulation(placedOrderCountArray) {
    HighCharts.chart("firstChart", {
      chart: {
        type: "column"
      },
      title: {
        text: "Order placed for last 2 weeks"
      },
      xAxis: {
        min: 0,
        type: 'category',
        labels: {
          rotation: 0,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Placed Orders'
        }
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        column: {
          colorByPoint: true
        }
      },
      series: [{
        name: 'Order',
        colors: ['#4be6d8', '#5f69f0'],
        data: placedOrderCountArray
      }]
    } as any);
  }

  dailySalesCount(totalSalesCount, growthPercentage, totalSales) {
    HighCharts.chart("secondChart", {
      title: {
        text: 'Current Week Total Sales ₹' + totalSales
      },
      subtitle: {
        text: 'Sales growth percentage from last week ' + growthPercentage + '%<br> increase and decrease sales amount per day'
      },
      xAxis: {
        type: 'datetime',
        labels: {
          formatter: function () {
            var chart = this.chart,
              weekdays = chart.options.lang.weekdays,
              day = new Date(this.value).getDay();

            return weekdays[day];
          }
        }
      },
      yAxis: {
        title: {
          text: 'Sales Amount'
        }
      },
      tooltip: {
        formatter: function () {
          var weekdays = this.series.chart.options.lang.weekdays,
            day = new Date(this.x).getDay();

          return weekdays[day] + ': ' + '₹' + this.y;
        }
      },
      legend: {
        enabled: false
      },
      series: [{
        data: totalSalesCount
      }]
    } as any);
  }


  dailyOrdersCount(totalSalesCount, growthPercentage, totalSales) {
    HighCharts.chart("thirdChart", {
      title: {
        text: 'Current Week Total Orders ' + totalSales
      },
      subtitle: {
        text: 'Orders growth percentage from last week ' + growthPercentage + '%<br> increase and decrease number of orders per day'
      },
      xAxis: {
        type: 'datetime',
        labels: {
          formatter: function () {
            var weekdays = this.chart.options.lang.weekdays,
              day = new Date(this.value).getDay();

            return weekdays[day];
          }
        }
      },
      yAxis: {
        title: {
          text: 'Daily Orders'
        }
      },
      tooltip: {
        formatter: function () {
          var weekdays = this.series.chart.options.lang.weekdays,
            day = new Date(this.x).getDay();

          return weekdays[day] + ': ' + this.y;
        }
      },
      legend: {
        enabled: false
      },
      series: [{
        data: totalSalesCount
      }]
    } as any);
  }
}
