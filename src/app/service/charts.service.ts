import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChartsService {

  constructor(private http: HttpClient) { }

  /**
   * @function getFirstGraphData
   * @description this function using for graph 1 
   * 
  */
  public getFirstGraphData(): Observable<any> {
    return this.http.get<any>('https://servercharts.herokuapp.com/api/v1/orderList')
  }
  /**
   * @function getSecondGraphData
   * @description this function using for graph 2 
   * 
  */
  public getSecondGraphData(): Observable<any> {
    return this.http.get<any>('https://servercharts.herokuapp.com/api/v1/orderSales')
  }
  /**
   * @function getThirdGraphData
   * @description this function using for graph 3 
   * 
  */
  public getThirdGraphData(): Observable<any> {
    return this.http.get<any>('https://servercharts.herokuapp.com/api/v1/orderPerWeek')
  }
}
